#!/bin/bash

# Ce script automatise la provision d'un serveur web avec nginx. Il prend un argument d'entrée $1 et renseigne le nom de la machine web dans le fichier de configuration nginx. Dès lors, en accédant au site web, le message "Machine : web $1" devrait apparaître.

echo 'Starting Provision: web'$1
sudo apt-get update
sudo apt-get install -y nginx
echo "<h1>Machine: web"$1 "</h1>" > /var/www/html/index.html
echo 'Provision web'$1 'complete'