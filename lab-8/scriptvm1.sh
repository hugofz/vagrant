#!/bin/bash

#Script de configuration du load balancer et de création d'un upstream backend nginx.

#1. Affichage de "Starting Provision :lb1"
echo 'Starting Provision: lb1'

#2. Mise à jour de la vm et installation de nginx
sudo apt-get update
sudo apt-get install -y nginx

#3. Configuration du load balancer et création de l'upstream backend
sudo service nginx stop

        #3.1. Suppression du fichier de configuration par défaut de nginx et création d'u nouveau fichier de configuration
sudo rm -rf /etc/nginx/sites-enabled/default
sudo touch /etc/nginx/sites-enabled/default

        #3.2. Création d'un upstream backend "testapp" comprenant les deux web servers.
        # server listen permet de définir le port 80 comme pour d'écoute par défaut. 
        # root/usr/share/nginx html précise le répertoire racine où les fichiers du site web sont localisés.
        # index index.html index.htm précise les fichiers index par ordre de priorité qui seront recherchés par nginx lorsqu'un répertoire est demandé. 
        # location / { proxy_pass http://testapp } fait en sorte que chaque fois qu'une requête sera adressée vers localhost, elle sera redirigée vers les serveurs backend définis dans testapp (load balancing) 
echo "upstream testapp {
        server 10.0.0.11;
        server 10.0.0.12;
}

server {
        listen 80 default_server;
        listen [::]:80 default_server ipv6only=on;

        root /usr/share/nginx/html;
        index index.html index.htm;

        # Make site accessible from http://localhost/
        server_name localhost;

        location / {
                proxy_pass http://testapp;
        }

}" >> /etc/nginx/sites-enabled/default

#4. Redémarrage dy service nginx.
sudo service nginx start
echo "Machine: lb1" > /var/www/html/index.html
echo 'Provision lb1 complete'