# vagrant



## Lab 1 : Découverte de Vagrant

Liste des commandes utilisées : 

- **`vagrant init`** : crée un fichier de configuration vagrantfile dans le répertoire courant. Ce fichier contient les paramètres de configuration de base pour une VM créée avec Vagrant. 
- **`vagrant validate`** : vérifie la syntaxe du vagrantfile. Si le fichier est correctement formaté, la commande vagrant validate ne renvoie aucune erreur.
- **`vagrant up`** : crée, démarre et provisionne la VM basée sur le vagrantfile. Dans notre cas : le vagrantfile n'ayant pas été configuré (la box n'ayant pas modifiée, le fichier "`base`" est reseigné par défaut et ne renvoie à rien), un message d'erreur est retourné. 
- **`vagrant status`** : affiche l'état des VM gérées par Vagrant dans le répertoire de travail actel. 
- **`vagrant global-status`** : affiche l'état global de toutes les VM gérées par Vagrant.
- **`vagrant ssh`** : permet de se connecter en ssh à la VM gérée par Vagrant (la VM doit être en cours d'exécution et située dans le répertoire courant)
- **`vagrant halt`** : arrête la VM. 
- **`vagrant destroy`** : supprime la VM. 
- **`vagrant box add ubuntu/trusty64`** : ajoute une nouvelle box (ici, la box ubuntu/trusty64) à Vagrant à partir du Vagrant Cloud. La box est alors téléchargée. Dès lors, il sera possible de modifier le vagrantfile en précisant "`config.vm.box = ubutnu/trusty64`" afin de la commande vagrant up lance la création, le démarrage et la provision d'une VM ubuntu. 
- **`vagrant init ubuntu/trusty64 -h`** : affiche l'aide sur la façon d'utiliser la commande `vagrant init`avec des options spécifiques pour initialiser un nouveau projet Vagrant avec la box ubuntu/trusty64.

## Lab 2 : Créer une VM

### Créer une VM ubuntu trusty64 v20190425.0.0
```
cd vagrant
mkdir lab-2 && cd lab-2
vagrant init
vagrant box add ubuntu/trusty64 --box.version 20190425.0.0
vim vagrantfile
    ...
    config.vm.box = "ubuntu/trusty64"
    config.vm.box_version = "20190425.0.0"
    ...
vagrant validate
```
Le téléchargement de la box requise et la configuration du vagrantfile peut être simplifiée par l'emploi de la commande `vagrant init ubuntu/trusty64 --box.version 20190425.0.0`.

### Démarrage de la machine virtuelle et connexion en ssh
```
vagrant up
vagrant ssh
```
### Installation de nginx et configuration du démarrage du service au lancement de la VM
```
sudo apt install nginx
sudo service nginx start
sudo update-rc.d nginx defaults
```
### Exportation de la machine virtuelle au format vagrant box (vbox)
```
exit 
vagrant package --output hugoasdram.box
```
### Upload de la vbox sur le Vagrant Cloud
```
vagrant cloud auth login --token MY_TOKEN
vagrant cloud provider create hugoasdram/nginx-version-v1 virtualbox 0.1.0
vagrant cloud publish hugoasdram/nginx-version-v1 0.1.0 virtualbox hugoasdram.box
```

## Lab 3 : Création d'un vagrantfile

### Création d'une VM Centos
```
cd vagrant
mkdir lab-3 && cd lab-3
vagrant init -m geerlingguy/centos7
```
### Configuration du vagrantfile

Le fichier vagrantfile relatif est disponible dans le repository **lab-3**. 
```
vagrant validate
vagrant up
```
### Lancement de la VM, isntallation de nginx et suppression de la VM
```
vagrant ssh
sudo yum install nginx
sudo poweroff
vagrant destroy
```

## Lab 4 : Déploiement d'un serveur web

Afin d'installer automatiquement nginx au lancement de la vm, deux solutions d'offrent à nous : 
- Rédiger un script bash auquel on fait référence dans notre vagrantfile, 
- Placer des commandes d'installation directement dans le vagrantfile.
Ici, nous privilégierons cette seconde option et préfèrerons rédiger un script bash dans l'un des lab suivant. 

Ainsi, il convient d'ajouter les commandes suivantes à notre vagrantfile : 

```
  #Installation et démarrage automatique de nginx
  config.vm.provision "shell", inline: <<-SHELL
    sudo yum update
	sudo yum upgrade
	sudo yum install nginx -y
    sudo systemctl start nginx
	sudo systemctl enable nginx
  SHELL
```

*Le vagrantfile en question est disponible dans le dossier lab-4.*

## Lab 5 : Déploiement d'un serveur web

Il convient désormais de créer 3 vm à partir d'un seul vagrant file. Nous allons variabiliser la RAM, le CPU et l'adresse IP des vm et rédiger un script bash provision.sh permettant d'installer nginx sur 2 des 3 machines. 

Afin de créer ces vm, nous allons intégrer une loop à notre vagrantfile par le biais de la commande ```(1..3).each do |i|```. Voici le bloc permettant de créer les trois machines, de leur attribuer trois adresses ip en 10.0.0.10, 10.0.0.11 et 10.0.0.12 ainsi qu'en appliquant le script provision.sh aux deux dernières vm : 
```
#Loop pour la création des VM
	(1..3).each do |i|
		config.vm.define "vm#{i}" do |node|
		
			#Configuration réseau
			node.vm.network "private_network", ip: "#{ip_base}#{i + 9}"
		
			#Configuration de la RAM et cpu
			node.vm.provider "virtualbox" do |vb|
				vb.memory = ram
				vb.cpus = cpu
			end
	
			#Provisionnement avec un script Bash des vm2 et vm3
			if i == 2 || i == 3
				node.vm.provision "shell", path: "provision.sh"
			end
		end 
	end
```
Pour ce qui est du script provision.sh, il est nécessaire qu'il soit située dans le même dossier que notre vagrantfile. Nous le renseignerons comme suit : 
```
#!/bin/bash

#Mise à jour du système
sudo apt update -y
sudo apt upgrade -y

#Installation de nginx
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
```
Le vagrantfile et le script bash sont disponibles dans le dossier lab-5. 

## Lab 6 : Vagrant plugins

Dans ce lab, nous allons utiliser un plugin permettant de modifier automatiquement le fichier /etc/hosts de l'hôte afin d'associer l'adresse IP d'une VM à son hostname. Ainsi, nous pourrons accéder à notre serveur nginx en tapant seulement le hostname de la VM hébergeant nginx depuis le navigateur de notre machine hôte. 

Cependant, l'extension vagrant-hostsupdater n'est plus maintenue depuis de nombreuses années et ne fonctionne plus. Nous allons donc lui préférer l'extension goodhosts. 

Il convient alors d'installer le plugin en utilisant la commande ```python
vagrant plugin install vagrant-goodhosts```. 

Dès lors, il est possible d'insérer le bloc suivant dans notre vagrantfile : 
```
#Configuration hostname
	node.vm.hostname = "vm#{i}"
	node.goodhosts.aliases = ["vm#{i}"]
```
Le vagrantfile est disponible dans le dossier lab-6.

## Lab 7 : Webapp folder

