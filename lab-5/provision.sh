#!/bin/bash

#Mise à jour du système
sudo apt update -y
sudo apt upgrade -y

#Installation de nginx
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx